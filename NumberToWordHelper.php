<?php

class NumberToWordHelper
{
    public static function primeCheck($number)
    {
        if ($number == 1)
            return false;
        for ($i = 2; $i <= $number / 2; $i++) {
            if ($number % $i == 0)
                return false;
        }
        return true;
    }

    public static function printDivisors($n)
    {
        $arr = [];
        $j = 0;

        //loop from 1 to sqrt(n)
        for ($i = 1; $i <= sqrt($n); $i++) {
            if ($n % $i == 0) {
                if ($n / $i == $i)
                    echo "$i ";
                else {
                    echo "$i ";
                    //storing the large number of a pair
                    $arr[$j++] = (int)($n / $i);
                }
            }
        }

        //printing stored large numbers of pairs
        for ($i = count($arr) - 1; $i >= 0; $i--)
            echo $arr[$i] . " ";
        echo "\n";
    }

    public static function getUniqueRandomNumbersWithinRange($min, $max, $quantity)
    {
        $numbers = range($min, $max);
        shuffle($numbers);

        return array_slice($numbers, 0, $quantity);
    }
}