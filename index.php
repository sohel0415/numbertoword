<?php

require 'vendor/autoload.php';
require 'country_with_currency.php';
require 'NumberToWordHelper.php';

use NumberToWords\NumberToWords;

$country_code = 'en';
if (isset($_SERVER["HTTP_CF_IPCOUNTRY"])) {
    $country_code = $_SERVER["HTTP_CF_IPCOUNTRY"];
}

$numberToWords = new NumberToWords();
$numberTransformer = $numberToWords->getNumberTransformer($country_code);

$randomNumbers = NumberToWordHelper::getUniqueRandomNumbersWithinRange(0, 100, 16);
$baseUrl = '/arsh-arora/numtoword/';

$queryString = $_SERVER['QUERY_STRING'];
$suffixUrl = "-in-words-spellings.html";
$number = 1;
$isHomePage = true;
preg_match("^(\d+)" . $suffixUrl . "^", $queryString, $m);

if ($m && isset($m[1])) {
    $number = abs(intval($m[1]));
    $isHomePage = false;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>Number To Word</title>
    <meta name="author" content="name"/>
    <meta name="description" content="description here"/>
    <meta name="keywords" content="keywords,here"/>
    <script src="<?= $baseUrl ?>assets/tailwindcss.css"></script>
    <!--Replace with your tailwind.css once created-->
    <style>
        .bg-purple-custom {
            background-color: purple;
        }
    </style>
</head>
<body class="bg-white font-sans leading-normal tracking-normal">
<nav id="header" class="fixed w-full z-10 top-0 bg-purple-custom">

    <div id="progress" class="h-1 z-20 top-0"
         style="background:linear-gradient(to right, #4dc0b5 var(--scroll), transparent 0);"></div>

    <div class="w-full md:max-w-4xl mx-auto flex flex-wrap items-center justify-between mt-0 py-3">

        <div class="pl-4">
            <a class="text-white text-base no-underline hover:no-underline font-extrabold text-xl"
               href="<?= $baseUrl ?>">
                Number To Word
            </a>
        </div>
    </div>
</nav>
<!--Container-->
<div class="container w-full md:max-w-4xl mx-auto pt-20">

    <div class="overflow-hidden rounded-lg shadow-lg bg-purple-400">
        <div class="px-6 py-4">
            <h4 class="w-full mb-3 text-xl font-semibold tracking-tight text-gray-800">
                How to Write, Spell, Say <?= $number ?> in English Words
            </h4>
            <h2 class="w-full text-xl leading-normal text-gray-700 text-center py-6 px-12 bg-purple-50">
                <?= ucwords(str_replace("-", " ", $numberTransformer->toWords($number))) ?>
            </h2>
            <div class="w-full flex justify-center">
                <div class="flex justify-start items-center border-b border-teal-500 w-1/2 bg-white mt-2">
                    <h4 class="w-1/3 bg-purple-custom text-white px-2 py-2">Number: </h4>
                    <input class="appearance-none bg-white border-none w-1/3 text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none"
                           type="number" id="number" min="0" step="1" value="<?= $number ?>" aria-label="Number">
                    <button class="flex-shrink-0 w-1/3 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-1 px-2 mr-1 rounded"
                            type="button" onclick="nextPage()">
                        Search
                    </button>
                </div>
            </div>
        </div>
    </div>
    <?php if (!$isHomePage): ?>
        <?php if ($number < 100000): ?>
            <div class="overflow-hidden rounded-lg shadow-lg bg-purple-400 mt-4">
                <div class="px-6 py-4">
                    <h4 class="w-full mb-3 text-xl font-semibold tracking-tight text-gray-800">Facts about
                        number <?= $number ?></h4>
                </div>
            </div>
            <div class="overflow-hidden rounded-lg shadow-lg bg-purple-200 mt-1">
                <div class="px-6 py-4">
                    <h4 class="w-full mb-3 text-xl font-semibold tracking-tight text-gray-800">
                        Table of word <?= $number ?> (<?= $numberTransformer->toWords($number) ?>)
                    </h4>
                    <div class="flex flex-wrap text-left w-full leading-normal text-gray-700 py-6 px-12 bg-purple-50">
                        <h4 class="w-full">In mathematics, Tables are the multiplication of two numbers One number is on
                            one
                            row, one down a column.</h4>
                        <table class="mt-4 table-auto">
                            <?php for ($i = 1; $i <= 10; $i++): ?>
                                <tr>
                                    <td>
                                        <?= $number ?> (<?= $numberTransformer->toWords($number) ?>)
                                    </td>
                                    <td>*</td>
                                    <td>
                                        <?= $i ?> (<?= $numberTransformer->toWords($i) ?>)
                                    </td>
                                    <td>=</td>
                                    <td>
                                        <?= $number * $i ?> (<?= $numberTransformer->toWords($number * $i) ?>)
                                    </td>
                                </tr>
                            <?php endfor; ?>
                        </table>
                    </div>
                </div>
            </div>
            <div class="overflow-hidden rounded-lg shadow-lg bg-purple-200 mt-1">
                <div class="px-6 py-4">
                    <h4 class="w-full mb-3 text-xl font-semibold tracking-tight text-gray-800">
                        What is Square Root of <?= $number ?>?
                    </h4>
                    <div class="flex flex-wrap text-left w-full leading-normal text-gray-700 py-6 px-12 bg-purple-50">
                        <h4 class="w-full">A square root is a number that gives a result when it multiplies by itself.
                            It
                            has
                            two forms radical & exponential.</h4>
                        <ul class="w-full pl-4 pt-4 list-disc">
                            <li>Square Root of <?= $number ?>: <?= sqrt($number) ?></li>
                            <li>Square Root of <?= $number ?> in exponential form: (<?= $number ?>)<sup>1/2</sup> or
                                (<?= $number ?>)<sup>0.5</sup></li>
                            <li>Square Root of <?= $number ?> in radical form: &radic;<?= $number ?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="overflow-hidden rounded-lg shadow-lg bg-purple-200 mt-1">
                <div class="px-6 py-4">
                    <h4 class="w-full mb-3 text-xl font-semibold tracking-tight text-gray-800">What is a Rational
                        Number?</h4>
                    <div class="flex flex-wrap text-left w-full leading-normal text-gray-700 py-6 px-12 bg-purple-50">
                        <h4 class="w-full">In mathematics, the concept of rational numbers is known to refer to pointers
                            that
                            allow knowing the quotient between two integers. which is in the form of p/q. Examples of
                            rational
                            numbers are given below-</h4>
                        <table class="mt-4 table-fixed">
                            <tr>
                                <th class="w-24">p</th>
                                <th class="w-24">q</th>
                                <th class="w-48">p/q</th>
                                <th class="w-24">Rational</th>
                            </tr>
                            <tr>
                                <td><?= $number ?></td>
                                <td>2</td>
                                <td><?= $number ?>/2 = <?= $number / 2 ?></td>
                                <td>Rational</td>
                            </tr>
                            <tr>
                                <td><?= $number ?></td>
                                <td>100</td>
                                <td><?= $number ?>/100 = <?= $number / 100 ?></td>
                                <td>Rational</td>
                            </tr>
                            <tr>
                                <td><?= $number ?></td>
                                <td>0</td>
                                <td><?= $number ?>/0 = 0</td>
                                <td>Not Rational</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="overflow-hidden rounded-lg shadow-lg bg-purple-200 mt-1">
                <div class="px-6 py-4">
                    <h4 class="w-full mb-3 text-xl font-semibold tracking-tight text-gray-800">Is <?= $number ?> a prime
                        number?</h4>
                    <div class="flex flex-wrap text-left w-full leading-normal text-gray-700 py-6 px-12 bg-purple-50">
                        <?php if (NumberToWordHelper::primeCheck($number)): ?>
                            <h4 class="w-full">Yes - Because <?= $number ?> is divisible only by itself</h4>
                        <?php else: ?>
                            <h4 class="w-full">NO - Because <?= $number ?> is divisible by numbers other than itself and
                                1</h4>
                        <?php endif; ?>
                        <p class="mt-4">
                            A prime number has a unique identity that these types of numbers can only divide by
                            themselves &
                            1
                            (examples- 2, 3, 5, 7, 11)
                        </p>
                    </div>
                </div>
            </div>
            <div class="overflow-hidden rounded-lg shadow-lg bg-purple-200 mt-1">
                <div class="px-6 py-4">
                    <h4 class="w-full mb-3 text-xl font-semibold tracking-tight text-gray-800">Is <?= $number ?> an even
                        number?</h4>
                    <div class="flex flex-wrap text-left w-full leading-normal text-gray-700 py-6 px-12 bg-purple-50">
                        <?php if ($number % 2 == 0): ?>
                            <h4 class="w-full">Yes - <?= $number ?> is an even number.</h4>
                        <?php else: ?>
                            <h4 class="w-full">No - <?= $number ?> is not an even number.</h4>
                        <?php endif; ?>
                        <p class="mt-4">
                            Numbers, which can be completely divisible by 2. It is called an even number. (Example = 2,
                            4,
                            6, 8 …..∞)
                        </p>
                    </div>
                </div>
            </div>
            <div class="overflow-hidden rounded-lg shadow-lg bg-purple-200 mt-1">
                <div class="px-6 py-4">
                    <h4 class="w-full mb-3 text-xl font-semibold tracking-tight text-gray-800">Is <?= $number ?> an odd
                        number?</h4>
                    <div class="flex flex-wrap text-left w-full leading-normal text-gray-700 py-6 px-12 bg-purple-50">
                        <?php if ($number % 2 != 0): ?>
                            <h4 class="w-full">Yes - <?= $number ?> Is an odd number</h4>
                        <?php else: ?>
                            <h4 class="w-full">No - <?= $number ?> is not an odd number.</h4>
                        <?php endif; ?>
                        <p class="mt-4">
                            Numbers that can not completely divisible by 2. are called odd numbers. (Example= 1, 3, 5,
                            51
                            ….. ∞)
                        </p>
                    </div>
                </div>
            </div>
            <div class="overflow-hidden rounded-lg shadow-lg bg-purple-200 mt-1">
                <div class="px-6 py-4">
                    <h4 class="w-full mb-3 text-xl font-semibold tracking-tight text-gray-800">What are the factors of
                        <?= $number ?>?</h4>
                    <div class="flex flex-wrap text-left w-full leading-normal text-gray-700 py-6 px-12 bg-purple-50">
                        <?php if (NumberToWordHelper::primeCheck($number)): ?>
                            <h4 class="w-full">The number <?= $number ?> is a prime number. Being a prime
                                number, <?= $number ?> has just two factors, 1
                                and
                                <?= $number ?>.</h4>
                        <?php else: ?>
                            <h4 class="w-full">The number <?= $number ?> is not a prime number. The factors
                                of <?= $number ?> are <?php NumberToWordHelper::printDivisors($number); ?></h4>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="overflow-hidden rounded-lg shadow-lg bg-purple-200 mt-1">
                <div class="px-6 py-4">
                    <h4 class="w-full mb-3 text-xl font-semibold tracking-tight text-gray-800">What are the 10 multiples
                        of
                        <?= $number ?>?</h4>
                    <div class="flex flex-wrap text-left w-full leading-normal text-gray-700 py-6 px-12 bg-purple-50">
                        <h4 class="w-full">The first 10 multiples of <?= $number ?> are
                            <?= $number * 1 ?>, <?= $number * 2 ?>, <?= $number * 3 ?>, <?= $number * 4 ?>,
                            <?= $number * 5 ?>, <?= $number * 6 ?>, <?= $number * 7 ?>, <?= $number * 8 ?>,
                            <?= $number * 9 ?>, <?= $number * 10 ?>.</h4>
                    </div>
                </div>
            </div>
            <div class="overflow-hidden rounded-lg shadow-lg bg-purple-200 mt-1">
                <div class="px-6 py-4">
                    <h4 class="w-full mb-3 text-xl font-semibold tracking-tight text-gray-800">How To Write &
                        Spell <?= $number ?> In Currency??</h4>
                    <div class="flex flex-wrap text-left w-full leading-normal text-gray-700 py-6 px-12 bg-purple-50">
                        <table class="mt-4 table-auto">
                            <tr>
                                <th class="px-4">Country and Currency</th>
                                <th class="px-4">Currency Code</th>
                                <th class="px-4">Graphic Image</th>
                                <th class="px-4">Symbol Code</th>
                                <th class="px-4">Currency</th>
                                <th class="px-4">Currency in word</th>
                            </tr>
                            <?php foreach ($currency_list as $code => $item): ?>
                                <tr>
                                    <td class="px-4"><?= $item['name'] ?></td>
                                    <td class="px-4"><?= $code ?></td>
                                    <td class="px-4"><?= $item['symbol'] ?></td>
                                    <td class="px-4"><?= $item['symbol'] ?></td>
                                    <td class="px-4"><?= $number . ' ' . $code ?></td>
                                    <td class="px-4"><?= ucwords(str_replace("-", " ", $numberTransformer->toWords($number))) . ' ' . $code ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="overflow-hidden rounded-lg shadow-lg bg-purple-400 mt-4">
            <div class="px-6 py-4">
                <h4 class="w-full mb-3 text-xl font-semibold tracking-tight text-gray-800">Other Numbers</h4>
                <div class="flex flex-wrap w-full leading-normal text-gray-700 text-center py-6 px-12 bg-purple-50">
                    <?php for ($otherNumber = $number + 1; $otherNumber <= $number + 22; $otherNumber++): ?>
                        <a href="<?= $otherNumber . $suffixUrl ?>"
                           class="w-1/6 p-4 text-bold hover:underline" style="color: #337ab7;"><?= $otherNumber ?></a>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($isHomePage): ?>
        <div class="overflow-hidden rounded-lg shadow-lg bg-purple-400 mt-4">
            <div class="px-6 py-4">
                <h4 class="w-full mb-3 text-xl font-semibold tracking-tight text-gray-800">Random Numbers List</h4>
                <div class="flex flex-wrap w-full leading-normal text-gray-700 text-center py-6 px-12 bg-purple-50">
                    <?php foreach ($randomNumbers as $randomNumber): ?>
                        <a href="<?= $randomNumber . $suffixUrl ?>"
                           class="w-1/6 p-4 text-bold hover:underline" style="color: #337ab7;">
                            <?= $randomNumber ?>
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
<!--/container-->
<footer class="bg-white border-t border-gray-400 shadow mt-4 bg-teal-900">
    <div class="container max-w-4xl mx-auto flex py-4">
        <div class="text-center"> @2022 numtoword.com. All rights reserved.</div>
    </div>
</footer>
<script>
    let h = document.documentElement,
        b = document.body,
        st = 'scrollTop',
        sh = 'scrollHeight',
        progress = document.querySelector('#progress'),
        scroll;
    let scrollPosition = window.scrollY;
    let header = document.getElementById("header");

    document.addEventListener('scroll', function () {
        scroll = (h[st] || b[st]) / ((h[sh] || b[sh]) - h.clientHeight) * 100;
        progress.style.setProperty('--scroll', scroll + '%');

        scrollPosition = window.scrollY;

        if (scrollPosition > 10) {
            header.classList.add("shadow");
        } else {
            header.classList.remove("shadow");
        }
    });

    function nextPage() {
        let url = '<?= $baseUrl ?>' + Math.round(document.getElementById('number').value) + '<?= $suffixUrl ?>';
        window.location.replace(url);
    }</script>
</body>
</html>
