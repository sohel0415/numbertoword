<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => 'sohel0415/numtoword',
        'dev' => true,
    ),
    'versions' => array(
        'kwn/number-to-words' => array(
            'pretty_version' => '2.2.0',
            'version' => '2.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../kwn/number-to-words',
            'aliases' => array(),
            'reference' => '4bc4b334d1ef7fe34406472f570aff5f9a7b2107',
            'dev_requirement' => false,
        ),
        'sohel0415/numtoword' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
    ),
);
